﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebstoreCspClient.Functions;


using WebstoreCspClient;
using WebstoreCspClient.Functions;
using WebstoreCspClient.Common;

namespace WebstoreCspClient
{
    class Program
    {
        static readonly List<IEKeyFunction> FuncList = new List<IEKeyFunction>
        {
            new GetSubscribersId(),
            new GetSubscriber(),
            new GetSubscriptionsId(),
            new GetSubscription(),
            new StartSubscription(),
            new StopSubscription(),
            new ChangeQuantityOfSubscription(),
            new ChangeEndDate(),
            new IsUserActivate(),
            new ActivateUser(),
            new ProdList(),
            new AddSubscription()
        };
        static void Main(string[] args)
        {
            //var eKeyService = new EKeyService("https://localhost:44306/Version2/Service/B2BServiceV2.svc", "testme", "qwerty");
            var eKeyService = new EKeyService("https://tayga:453/Version2/Service/B2BServiceV2.svc", "testme", "qwerty");
            bool cnt = true;
            while (cnt)
            {
                Console.WriteLine("Доступные методы:");
                foreach (var func in FuncList)
                {
                    Console.WriteLine(func.MenuTitle);
                }
                Console.WriteLine("exit - выход");
                var userResult = Console.ReadLine() ?? "";
                if (!userResult.Equals("exit", StringComparison.CurrentCultureIgnoreCase))
                {
                    var currFunc = FuncList.FirstOrDefault(x => x.IsCurrentFunction(userResult));
                    if (currFunc == null)
                        Console.WriteLine("Функция не найдена. Попробуйте еще раз");
                    else if (!currFunc.IsComplex)
                    {
                        var answers = new List<string>();
                        foreach (var question in currFunc.GetQuestions())
                        {
                            Console.WriteLine(question);
                            answers.Add(Console.ReadLine());
                        }
                        Console.WriteLine("================================");
                        try
                        {
                            Console.WriteLine(currFunc.CallAndGetResult(eKeyService, answers));
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("При вызове метода произошла ошибка:");
                            Console.WriteLine(ex.ToString());
                        }
                        Console.WriteLine("================================");
                    }
                    else
                    {
                        try
                        {
                            Console.WriteLine(currFunc.CallComplex(eKeyService));
                        }
                        catch(B2BException ex1)
                        {
                            Console.WriteLine("("+ex1.Code+") "+ex1.MessageText);
                        }
                        catch(Exception ex)
                        {
                            Console.WriteLine(ex.ToString());
                        }
                    }
                }
                else
                {
                    cnt = false;
                }
            }
        }
    }
}
