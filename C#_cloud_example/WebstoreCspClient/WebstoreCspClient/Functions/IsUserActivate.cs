﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebstoreCspClient.Common;

namespace WebstoreCspClient.Functions
{
    class IsUserActivate : IEKeyFunction
    {
        public List<string> GetQuestions()
        {
            return new List<string>
            {
                "Введите парт. номер",
                "Введите идентификатор подписчика"
            };
        }

        public string MenuTitle { get { return "76 - проверка активированности подписчика"; }}
        public bool IsCurrentFunction(string userInput)
        {
            return userInput.Equals("76");
        }

        public string CallAndGetResult(EKeyService service, List<string> answers)
        {
            return service.IsUserActivated(answers[0], answers[1]).Serialize();
        }

        public bool IsComplex { get { return false; }}
        public string CallComplex(EKeyService service)
        {
            throw new NotImplementedException();
        }
    }
}
