﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebstoreCspClient.Common;

namespace WebstoreCspClient.Functions
{
    class GetSubscribersId:IEKeyFunction
    {
        public List<string> GetQuestions()
        {
            return new List<string>
            {
                "Введите номер версии",
                "Загрузить только для логина (введите 1) или для всего юрлица (нажмите Enter) ?"
            };
        }

        public string MenuTitle { get { return "1 - Получение списка id подписчиков"; } }

        public bool IsComplex
        {
            get
            {
                return false;
            }
        }

        public bool IsCurrentFunction(string userInput)
        {
            return userInput.Equals("1");
        }

        public string CallAndGetResult(EKeyService service, List<string> answers)
        {
            return service.GetSubscriberIds(int.Parse(answers[0]), answers[1] != "1").Serialize();
        }

        public string CallComplex(EKeyService service)
        {
            throw new NotImplementedException();
        }
    }
}
