﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebstoreCspClient.Common;

namespace WebstoreCspClient.Functions
{
    class GetSubscription : IEKeyFunction
    {
        public List<string> GetQuestions()
        {
            return new List<string>
            {
                "Введите номер подписки"
            };
        }

        public string MenuTitle { get { return "4 - Получение подписки по номеру"; } }

        public bool IsComplex
        {
            get
            {
                return false;
            }
        }

        public bool IsCurrentFunction(string userInput)
        {
            return userInput.Equals("4");
        }

        public string CallAndGetResult(EKeyService service, List<string> answers)
        {
            return service.GetSubscription(answers[0]).Serialize();
        }

        public string CallComplex(EKeyService service)
        {
            throw new NotImplementedException();
        }
    }
}
