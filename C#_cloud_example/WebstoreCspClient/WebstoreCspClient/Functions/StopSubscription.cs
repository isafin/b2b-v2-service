using System;
using System.Collections.Generic;
using WebstoreCspClient.Common;

namespace WebstoreCspClient.Functions
{
    public class StopSubscription : IEKeyFunction
    {
        public List<string> GetQuestions()
        {
            return new List<string>
            {
                "������� ����� ��������",
                "����� ����������� �� ��������� ������������? ������� 1 ���� ��, 0 ���� ���"
            };
        }

        public string MenuTitle { get { return "11 - ���� ��������"; } }

        public bool IsComplex
        {
            get
            {
                return false;
            }
        }

        public bool IsCurrentFunction(string userInput)
        {
            return userInput.Equals("11");
        }

        public string CallAndGetResult(EKeyService service, List<string> answers)
        {
            return service.StopSubscription(answers[0], answers[1].Equals("1")).Serialize();
        }

        public string CallComplex(EKeyService service)
        {
            throw new NotImplementedException();
        }
    }
}