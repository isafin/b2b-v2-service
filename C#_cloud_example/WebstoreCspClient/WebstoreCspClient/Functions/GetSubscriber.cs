﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebstoreCspClient.Common;

namespace WebstoreCspClient.Functions
{
    class GetSubscriber:IEKeyFunction
    {
        public List<string> GetQuestions()
        {
            return new List<string>
            {
                "Введите id подписчика"
            };
        }

        public string MenuTitle { get { return "2 - Получение подписчика по id"; } }

        public bool IsComplex
        {
            get { return false; }
        }

        public bool IsCurrentFunction(string userInput)
        {
            return userInput.Equals("2");
        }

        public string CallAndGetResult(EKeyService service, List<string> answers)
        {
            return service.GetSubscriber(answers[0]).Serialize();
        }

        public string CallComplex(EKeyService service)
        {
            throw new NotImplementedException();
        }
    }
}
