﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebstoreCspClient.Common;

namespace WebstoreCspClient.Functions
{
    class GetSubscriptionsId:IEKeyFunction
    {
        public List<string> GetQuestions()
        {
            return new List<string>
            {
                "Введите номер версии",
                "Загрузить только для логина (введите 1) или для всего юрлица (нажмите Enter) ?"
            };
        }

        public string MenuTitle { get { return "3 - Получение списка id подписок"; } }

        public bool IsComplex
        {
            get { return false; }
        }

        public bool IsCurrentFunction(string userInput)
        {
            return userInput.Equals("3");
        }

        public string CallAndGetResult(EKeyService service, List<string> answers)
        {
            return service.GetSubscriptionsId(int.Parse(answers[0]), answers[1] != "1").Serialize();
        }

        public string CallComplex(EKeyService service)
        {
            throw new NotImplementedException();
        }
    }
}
