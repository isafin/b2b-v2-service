﻿using System;
using System.Collections.Generic;
using WebstoreCspClient.Common;

namespace WebstoreCspClient.Functions
{
    public class StartSubscription : IEKeyFunction
    {
        public List<string> GetQuestions()
        {
            return new List<string>
            {
                "Введите номер подписки",
                "Вызов выполняется от конечного пользователя? Введите 1 если да, 0 если нет"
            };
        }

        public string MenuTitle { get { return "10 - старт подписки"; } }

        public bool IsComplex
        {
            get
            {
                return false;
            }
        }

        public bool IsCurrentFunction(string userInput)
        {
            return userInput.Equals("10");
        }

        public string CallAndGetResult(EKeyService service, List<string> answers)
        {
            return service.StartSubscription(answers[0], answers[1].Equals("1")).Serialize();
        }

        public string CallComplex(EKeyService service)
        {
            throw new NotImplementedException();
        }
    }
}