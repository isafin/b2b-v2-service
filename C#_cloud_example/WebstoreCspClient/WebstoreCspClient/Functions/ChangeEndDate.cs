﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebstoreCspClient.Common;

namespace WebstoreCspClient.Functions
{
    public class ChangeEndDate : IEKeyFunction
    {
        public List<string> GetQuestions()
        {
            return new List<string>
            {
                "Введите номер подписки",
                "Введите новую дату окончания в формате \"dd.MM.yyyy\". Если новой даты нет, введите пустую строку"
            };
        }

        public string MenuTitle { get { return "13 - изменение даты окончания подписки"; } }

        public bool IsComplex
        {
            get
            {
                return false;
            }
        }

        public bool IsCurrentFunction(string userInput)
        {
            return userInput.Equals("13");
        }

        public string CallAndGetResult(EKeyService service, List<string> answers)
        {
            return service.ChangeSubscriptionEndDate(answers[0], string.IsNullOrEmpty(answers[1])
                ? (DateTime?)null
                : DateTime.ParseExact(answers[1], "dd.MM.yyyy", null)).Serialize();
        }

        public string CallComplex(EKeyService service)
        {
            throw new NotImplementedException();
        }
    }
}
