﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebstoreCspClient.Functions
{
    public class AddSubscription : IEKeyFunction
    {
        public List<string> GetQuestions()
        {
            return new List<string>
            {
                "Укажите номер заказа",
                "Укажите партномер для заказа",
                "Укажите MontSubscriberId",
                "Укажите количество seats"
            };
        }

        public string MenuTitle 
        {
            get { return "79 - Создание подписки"; }
        }
        
        public bool IsCurrentFunction(string userInput)
        {
            return userInput.Equals("79");
        }

        public string CallAndGetResult(EKeyService service, List<string> answers)
        {
            var rez =  service.AddOrderForSubscription(answers[0], int.Parse(answers[3]), answers[1], answers[2]);
            return "Создана подписка с номером: " + rez.OrderLines[0].SubscriptionId;
        }

        public bool IsComplex { get { return false; } }
        public string CallComplex(EKeyService service)
        {
            throw new NotImplementedException();
        }
    }
}
