﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebstoreCspClient.Functions
{
    public class ProdList : IEKeyFunction
    {
        public List<string> GetQuestions()
        {
            return new List<string>
            {
                
            };
        }

        public string MenuTitle {
            get { return "78 - Список предложений подписок"; }
        }
        public bool IsCurrentFunction(string userInput)
        {
            return userInput.Equals("78");
        }

        public string CallAndGetResult(EKeyService service, List<string> answers)
        {
            service.GetAllProducts().PriceList.ToList().Where(z=>z.ProductType == 2).ToList().ForEach(x=>Console.WriteLine("Партномер: "+ x.PartNum));
            return "";
        }

        public bool IsComplex { get { return false; } }
        public string CallComplex(EKeyService service)
        {
            throw new NotImplementedException();
        }
    }
}
