using System;
using System.Collections.Generic;
using WebstoreCspClient.Common;

namespace WebstoreCspClient.Functions
{
    public class ChangeQuantityOfSubscription : IEKeyFunction
    {
        public List<string> GetQuestions()
        {
            return new List<string>
            {
                "������� ����� ��������",
                "����� ����������� �� ��������� ������������? ������� 1 ���� ��, 0 ���� ���",
                "������� ����� ����������(����� �����)"
            };
        }

        public string MenuTitle { get { return "12 - ��������� ���������� ������������� ��������"; } }

        public bool IsComplex
        {
            get
            {
               return false;
            }
        }

        public bool IsCurrentFunction(string userInput)
        {
            return userInput.Equals("12");
        }

        public string CallAndGetResult(EKeyService service, List<string> answers)
        {
            return service.ChangeQuantityOfSubscription(answers[0], answers[1].Equals("1"), Int32.Parse(answers[2])).Serialize();
        }

        public string CallComplex(EKeyService service)
        {
            throw new NotImplementedException();
        }
    }
}