﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebstoreCspClient.Common;
using WebstoreCspClient.WebstoreClient;

namespace WebstoreCspClient.Functions
{
    public class ActivateUser : IEKeyFunction
    {

        protected string PartNum;
        protected string SubscriberId;
        protected RegistrationDescriptionContract descForm;

        protected List<RegItemContract> ToSendForm()
        {
            var fields = new List<RegItemContract>();
            foreach (var desc in descForm.RegDescriptionFields)
            {
                fields.Add(new RegItemContract()
                {
                    FieldName = desc.FieldName,
                    FieldValue = desc.FieldsValue
                });
            }
            return fields;
        }

        public bool IsComplex
        {
            get
            {
                return true;
            }
        }

        public string MenuTitle
        {
            get
            {
                return "77 - активация пользователя";
            }
        }

        public string CallAndGetResult(EKeyService service, List<string> answers)
        {
            throw new NotImplementedException();
        }

        protected void GetActivationForm(EKeyService service)
        {
            descForm = service.GetActivationForm(PartNum, SubscriberId, "RU");
            foreach (var fild in descForm.RegDescriptionFields)
            {
                Console.WriteLine("Заполните " + fild.FieldTitle + " (" + fild.FieldDescription + "):");
                if ((fild.AvaliableValues != null) && (fild.AvaliableValues.Count() > 0))
                {
                    Console.WriteLine("Допустимые значения:");
                    foreach (var t in fild.AvaliableValues)
                    {
                        Console.WriteLine(t);
                    }
                }
                fild.FieldsValue = Console.ReadLine();
            }
        }

        public string CallComplex(EKeyService service)
        {
            Console.WriteLine("Введите пратномер:");
            PartNum = Console.ReadLine();
            Console.WriteLine("Введите код подписчика(GUID):");
            SubscriberId = Console.ReadLine();
            try
            {
                var activated = service.IsUserActivated(PartNum, SubscriberId);
                if (activated)
                {
                    return "Пользователь с указанным ID уже активирован";
                }
            }
            catch (Exception ex)
            {
                //do nothing
            }
            GetActivationForm(service);
            
            descForm =  service.ActivateUser(ToSendForm(), PartNum, SubscriberId, "RU");
            while(descForm.HasError)            
            {
                Console.WriteLine("Во время отправки формы, были обнаружены неправильно заполненные поля:");
                var cont = true;
                while (cont)
                {
                    Console.WriteLine("Хотите исправить и продолжить? [Y][N]");
                    var k = Console.ReadLine();
                    if (k.ToUpper().Equals("Y"))
                        cont = false;
                    if (k.ToUpper().Equals("N"))
                    {
                        cont = false;
                        return "Операция отмена";
                    }
                }
                
                foreach (var eitem in descForm.RegDescriptionFields.Where(x => x.HasError).ToList())
                {
                    Console.WriteLine("Заполните " + eitem.FieldTitle + " (" + eitem.FieldDescription + "):");
                    if ((eitem.AvaliableValues != null) && (eitem.AvaliableValues.Count() > 0))
                    {
                        Console.WriteLine("Допустимые значения:");
                        foreach (var t in eitem.AvaliableValues)
                        {
                            Console.WriteLine(t);
                        }
                    }
                    eitem.FieldsValue = Console.ReadLine();
                }
                descForm = service.ActivateUser(ToSendForm(), PartNum, SubscriberId, "RU");
            }

            Console.WriteLine("Пользователь с ид "+SubscriberId+" успешно создан");
            return PartNum;
        }

       

        public List<string> GetQuestions()
        {
            throw new NotImplementedException();
        }

        public bool IsCurrentFunction(string userInput)
        {
            return userInput.Equals("77");
        }
    }
}
