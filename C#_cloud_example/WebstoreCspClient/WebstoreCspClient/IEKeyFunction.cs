using System.Collections.Generic;

namespace WebstoreCspClient
{
    public interface IEKeyFunction
    {
        List<string> GetQuestions();
        string MenuTitle { get; }
        bool IsCurrentFunction(string userInput);
        string CallAndGetResult(EKeyService service, List<string> answers);
        bool IsComplex { get; }
        string CallComplex(EKeyService service);
    }
}