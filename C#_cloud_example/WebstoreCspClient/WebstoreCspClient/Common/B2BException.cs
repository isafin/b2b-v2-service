using System;
using System.Collections.Generic;

namespace WebstoreCspClient.Common
{
    public class B2BException : Exception
    {
        public int Code { get; private set; }
        public string MessageText { get; private set; }
        public List<B2BInnerError> InnerErrors { get; private set; }
        public B2BException(int code, string message, List<B2BInnerError> innerErrors = null)
            : base("code: " + code + ". message: " + message)
        {
            MessageText = message;
            Code = code;
            InnerErrors = innerErrors;
        }

        public override string ToString()
        {
            return base.ToString() + "Inner Errors: " + (InnerErrors != null ? InnerErrors.Serialize() : "");
        }
    }
}