namespace WebstoreCspClient.Common
{
    public class B2BInnerError
    {
        public int Code { get; set; }
        public string MessageText { get; set; }
        public string EntityName { get; set; }
        public string FieldName { get; set; }
    }
}