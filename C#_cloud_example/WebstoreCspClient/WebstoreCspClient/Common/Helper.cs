using System.IO;
using System.Xml.Serialization;

namespace WebstoreCspClient.Common
{
    public static class Helper
    {
        public static string Serialize<T>(this T toSerialize)
        {
            var serialiser = new XmlSerializer(typeof(T));
            var sw = new StringWriter();
            serialiser.Serialize(sw, toSerialize);
            return sw.ToString();
        }
    }
}