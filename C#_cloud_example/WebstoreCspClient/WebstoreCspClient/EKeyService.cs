﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Text;
using System.Xml;
using WebstoreCspClient.Common;
using WebstoreCspClient.WebstoreClient;

namespace WebstoreCspClient
{
    public sealed class EKeyService
    {
        private string _address;
        private string _login;
        private string _password;

        public EKeyService(string address, string login, string password)
        {
            _password = password;
            _login = login;
            _address = address;
        }

        private IB2BServiceV2 GetClient()
        {
            IB2BServiceV2 srv = null;
            var bnd = new WSHttpBinding(SecurityMode.TransportWithMessageCredential, false);
            ServicePointManager.CertificatePolicy = new TrustAllCertificatePolicy();
            var endPoint = new EndpointAddress(_address);
            var fileChannel = new ChannelFactory<IB2BServiceV2>(bnd, endPoint);
            bnd.OpenTimeout = new TimeSpan(0, 1, 0, 0);
            bnd.CloseTimeout = bnd.OpenTimeout;
            bnd.ReceiveTimeout = new TimeSpan(0, 10, 0);
            bnd.SendTimeout = bnd.ReceiveTimeout;
            bnd.MaxReceivedMessageSize = 2147483647;
            bnd.MaxBufferPoolSize = 2147483647;
            bnd.BypassProxyOnLocal = false;
            bnd.AllowCookies = false;
            bnd.TransactionFlow = false;
            bnd.TextEncoding = Encoding.UTF8;
            bnd.ReaderQuotas = new XmlDictionaryReaderQuotas() { MaxBytesPerRead = Int32.MaxValue };
            bnd.ReaderQuotas.MaxArrayLength = 2147483647;
            bnd.ReaderQuotas.MaxBytesPerRead = 2147483647;
            bnd.ReaderQuotas.MaxStringContentLength = 2147483647;
            bnd.ReaderQuotas.MaxNameTableCharCount = 2147483647;
            bnd.ReaderQuotas.MaxDepth = 2147483647;
            bnd.MaxReceivedMessageSize = 2147483647;
            bnd.MaxBufferPoolSize = 512000;
            bnd.Security.Message.ClientCredentialType = MessageCredentialType.UserName;
            bnd.Security.Transport.ClientCredentialType = HttpClientCredentialType.None;
            bnd.Security.Message.EstablishSecurityContext = false;
            bnd.Security.Message.NegotiateServiceCredential = false;
            fileChannel.Credentials.UserName.UserName = _login;
            fileChannel.Credentials.UserName.Password = _password;
            srv = fileChannel.CreateChannel();
            return srv;
        }
        private T GetProperty<T>(object obj, string fieldName)
        {
            return (T)obj.GetType().GetProperty(fieldName).GetValue(obj);
        }
        private T CallMethod<T>(Func<IB2BServiceV2, T> methodFunc)
        {
            T res;
            IB2BServiceV2 service = null;
            try
            {
                service = GetClient();
                res = methodFunc(service);
            }
            catch (Exception ex)
            {
                throw new B2BException(-1, ex.ToString());
            }
            finally
            {
                if (service != null)
                    ((ICommunicationObject)service).Close();
            }
            if (GetProperty<bool>(res, "IsError"))
            {
                throw new B2BException(GetProperty<int>(res, "ErrorCode"), GetProperty<string>(res, "ErrorText"),
                    GetProperty<ErrorContract[]>(res, "Errors").Select(x => new B2BInnerError
                    {
                        Code = x.ErrorCode,
                        EntityName = x.EntityName,
                        FieldName = x.FieldName,
                        MessageText = x.ErrorText
                    }).ToList());
            }
            return res;
        }
        public SubscriptionContract StartSubscription(string subscriptionNum, bool byEndUser)
        {
            return CallMethod(client => client.StartSubscription(subscriptionNum, byEndUser)).Data;
        }
        public SubscriptionContract StopSubscription(string subscriptionNum, bool byEndUser)
        {
            return CallMethod(client => client.StopSubscription(subscriptionNum, byEndUser)).Data;
        }
        public SubscriptionContract ChangeQuantityOfSubscription(string subscriptionNum, bool byEndUser, int newQuantity)
        {
            return CallMethod(client => client.ChangeQuantityOfSubscription(subscriptionNum, byEndUser, newQuantity)).Data;
        }
        public SubscriptionContract ChangeSubscriptionEndDate(string subscriptionNum, DateTime? newEndDate)
        {
            var t = newEndDate == null ? null : newEndDate.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffffff");
            return CallMethod(client => client.ChangeSubscriptionEndDate(subscriptionNum, t)).Data;
        }
        public ChangedIdsContractOfguid GetSubscriberIds(int version, bool byAllLogins)
        {
            return CallMethod(client => client.GetSubscribersId(version, byAllLogins)).Data;
        }
        public WebstoreSubscriberContract GetSubscriber(string id)
        {
            return CallMethod(client => client.GetSubscriber(id)).Data;
        }
        public ChangedIdsContractOfstring GetSubscriptionsId(int version, bool byAllLogins)
        {
            return CallMethod(client => client.GetSubscriptionsId(version, byAllLogins)).Data;
        }
        public SubscriptionContract GetSubscription(string id)
        {
            return CallMethod(client => client.GetSubscription(id)).Data;
        }

        public RegistrationDescriptionContract GetActivationForm(string partNum, string montSubscriberId, string langCode)
        {
            return CallMethod(client => client.GetActivationForm(partNum, montSubscriberId, langCode)).Data;
        }

        public RegistrationDescriptionContract ActivateUser(List<RegItemContract> form, string partNum, string montSubscriberId, string langCode)
        {
            return CallMethod(client => client.ActivateSubscriber(form.ToArray(), montSubscriberId, partNum, langCode)).Data;
        }

        public bool IsUserActivated(string partNum, string montSubscriberId)
        {
            return CallMethod(client => client.IsSubscriberActivated(montSubscriberId, partNum)).Data;
        }

        public PriceChangesContract GetAllProducts()
        {
            return CallMethod(client => client.GetPriceChanges(0)).Data;
        }

        public OrderContract AddOrderForSubscription(string orderNum, int quantity, string partNum, string montSubscriberId)
        {
            var order = new SendOrderContract()
                {
                    OrderNum = orderNum
                };
            var oLine = new SendOrderLineContract();
            oLine.Quantity = quantity;
            oLine.PartNum = partNum;
            oLine.OrderLineNum = orderNum + "-1";
            oLine.MontSubscriberId = new Guid(montSubscriberId);
            order.SendOrderLines = new[] {oLine};
            return CallMethod(client => client.AddOrder(order)).Data;
        }
    }
}